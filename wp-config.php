<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'studiobz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']Yo/x%EF,axLKKk|=&Hk6{A:W:7Mh<7=v:V[yHk_!./<k}y]np3xJ-:hL?t!Qf(S' );
define( 'SECURE_AUTH_KEY',  'zLrfp6Q^GwCU+hpQl|kRze,<9@&k7=-LxMaw$f{H?6eRx7(.0TjS02^@~*gfC68k' );
define( 'LOGGED_IN_KEY',    'N*f9+O*M{8##yo~DA;AnSltotpd&5OFMJJ!#]= ,X~w9>(ok4.ibokW_nUzb^Qz(' );
define( 'NONCE_KEY',        'B:TtFWbaR2RG cm.Qedl[6AE_e)bq+nTk[Z}|JQzSNsipxB=B]~BmT[HT$y]wo|m' );
define( 'AUTH_SALT',        'S)DlhM{tUc!bBGwgR55#!=`/0B&)4mxaV`hF8 L7XD7geE$L-h#U;;g!f?+uZV;l' );
define( 'SECURE_AUTH_SALT', 'Z,:L62u?P)}3vn&]#)`$[&y-ivF=?Vt9]`12cRI&m}~4ktSL%Nab6qf,}n.179}A' );
define( 'LOGGED_IN_SALT',   'Od&}Q9${gI8k@.$R!z8~SFM-.lBT^MtxU8+e$KKFM,XU.#KP`pN%SNjK0G]LG{}c' );
define( 'NONCE_SALT',       '7IM)1IT. xd6G(?s_OwHWI$cuI>ddCE@Kpq8&G@`yfz[m3&SC}@DU;~n92z,n.{v' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
