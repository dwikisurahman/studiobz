<div class="grid_6 alpha">
    <div class="footer_widget first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php else : ?>
            <h6><?php _e('Mudah dan Praktis', 'road-fighter'); ?></h6>
            <div class="textwidget"><?php _e('Melalui B-z Studio, Anda dapat melakukan pemesanan jasa pembuatan gambar eksterior dan interior memalui media elektronik dengan mudah', 'road-fighter'); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="grid_6 alpha">
    <div class="footer_widget first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php else : ?>
            <h6><?php _e('Hemat Biaya', 'road-fighter'); ?></h6>
            <div class="textwidget"><?php _e('B-Z Studio menyediakan berbagai macam layanan jasa dibidang arsitektur atau banguan dan interior.', 'road-fighter'); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="grid_6 alpha">
    <div class="footer_widget first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php else : ?>
            <h6><?php _e('Cepat, Tepat dan Akurat', 'road-fighter'); ?></h6>
            <div class="textwidget"><?php _e('Setiap Proyek dan proses akan ditangani oleh tenaga ahli B-Z Studio yang bekerja secara profesional dan handal dibidangnya.'); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="grid_6 alpha">
    <div class="footer_widget first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php else : ?>
            <h6><?php _e('Revisi Desain Flexibel', 'road-fighter'); ?></h6>
            <div class="textwidget"><?php _e('Perubahan dalam mendesain merupakan suatu yang biasa terjadi. Oleh karenanya kami, mengginkan kepuasan dengan revisi desain yang flexibel', 'road-fighter'); ?></div>
        <?php endif; ?>
    </div>
</div>